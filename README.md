# React storybook starter
A template project for react storybook

## Prepare

```console
brew install nodenv # add `eval "$(nodenv init -)"` to startup file of your shell 
nodenv install 10.16.0
nodenv local 10.16.0
yarn
```
